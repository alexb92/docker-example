import os
import json
from datetime import datetime
from http.server import BaseHTTPRequestHandler, HTTPServer


class JSONWebServer(BaseHTTPRequestHandler):
	def do_GET(self):
		try:
			self.send_response(200)
			self.send_header("Content-type", "application/json")
			self.send_header("Last-Update", datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
			self.end_headers()
			data = {"test": "test"}
			self.wfile.write(bytes(json.dumps(data), "utf-8"))
		except:
			pass


if __name__ == "__main__":
	# start webserver
	PORT = int(os.getenv("PORT", 8080))
	HOST = os.getenv("HOST", "localhost")

	webServer = HTTPServer((HOST, PORT), JSONWebServer)
	try:
		webServer.serve_forever()
	except KeyboardInterrupt:
		webServer.server_close()
