## Docker Test
***This repository serves to showcase a simple pipeline to dockerize python projects***

### Requirements
- Docker (preferably Docker Desktop -> GUI)
- Python 3.9

### Running the Example
1) IDE
   
   The script main.py is programmed to run without any issues locally. Since it would 
   usually pull variables from the environment if it was running in docker, PORT and HOST
   are provided in the script.
   

2) Docker
    
   To run the script in Docker, simply navigate to your project's root directory and open a console.
   This procedure can be done from an IDE (e.g. PyCharm) but can also be done in a standard terminal.
   Use the following command:
   ```shell
   docker-compose up
   ```
   Troubleshooting:

   A common error is not having the docker daemon running. You need to start up docker to
   be able to build images and run containers.
   

### Navigating to the webserver
   Follow this link to open the webserver: http://localhost:8080/
   

### Resources
- [Basic Docker Tutorial](https://www.docker.com/101-tutorial)  
- [Docker-Compose Tutorial](https://docs.docker.com/compose/gettingstarted/)
- [Python Base Images](https://hub.docker.com/_/python)
- [Dockerized Flask Project](https://gitlab.com/alexb92/flaskh)