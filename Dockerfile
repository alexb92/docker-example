# pull official base image
FROM python:3.9-slim

# copy project directory/ files into container
COPY ./server /test_server

# set work directory
WORKDIR /test_server

# update system dependencies
RUN apt-get update

# install requirements, if there are any
#RUN pip install -r requirements.txt

# define the action that is taken on container start
ENTRYPOINT python3.9 main.py